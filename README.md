# CuteButton
[![platform](https://img.shields.io/badge/platform-Android-yellow.svg)](https://www.android.com) [![API](https://img.shields.io/badge/API-21%2B-brightgreen.svg?style=flat)](https://android-arsenal.com/api?level=21) [![JitPack](https://img.shields.io/jitpack/v/github/ahmmedrejowan/CuteButton)](https://jitpack.io/#ahmmedrejowan/CuteButton) [![GitHub license](https://img.shields.io/github/license/ahmmedrejowan/CuteButton)](https://github.com/ahmmedrejowan/CuteButton/blob/master/LICENSE)  
<br/>
A Material Design Custom Button Library for Android

<br/>

## What's New
Check on Releases Tab for changes.
<br/>

## Purpose
CuteButton is a Custom Material Design Android Library which could be an alternative for Deault Button Library. 
It has everything as the Default Button library, but with a lot customizations.
<br/>

## Features 

<br/>

## Prerequisites

<br/>

## Usage

<br/>

## Contribute

Please fork this repository and contribute back using [pull requests](https://github.com/ahmmedrejowan/CuteButton/pulls).

Any contributions, large or small, major features, bug fixes, are welcomed and appreciated
but will be thoroughly reviewed .

Let me know which features you want in the future in `Request Feature` tab. 

If this project helps you a little bit, then give a to Star ⭐ the Repo. 

<br/>


## Credits

Created with Love by **K M Rejowan Ahmmed** (@ahmmedrejowan)

Reach me @
* [Github](https://github.com/ahmmedrejowan) 
* [LinkedIn](https://www.linkedin.com/in/ahmmedrejowan)
* [Twitter](https://twitter.com/ahmmedrejowan)
* [Facebook](https://facebook.com/ahmmedrejowan)
* [StackOverFlow](https://stackoverflow.com/users/9932194/k-m-rejowan-ahmmed)





## Licences 
* [Apache Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)

```
Copyright 2021 K M Rejowan Ahmmed (ahmmedrejowan)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```
